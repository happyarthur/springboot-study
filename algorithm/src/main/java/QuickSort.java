import java.util.Arrays;

public class QuickSort {


    public static void main(String[] args) {
        int[] a = {3, 4, 1, 4, 5, -1, 12, 54, 1, 0};
        quickSort(a, 0, a.length - 1);
        System.out.println(Arrays.toString(a));
    }

    public static void quickSort(int[] nums, int l, int h) {
        if (l >= h)
            return;
        int privotIndex = partition(nums, l, h);
        quickSort(nums, l, privotIndex - 1);
        quickSort(nums, privotIndex + 1, h);
    }

    public static int partition(int[] a, int l, int h) {
        int privot = a[l];
        int lp = l;
        int hp = h + 1;
        while (true) {
            while (hp > l && less(privot, a[--hp])) ;
            while (lp < h && less(a[++lp], privot)) ;
            if (lp >= hp)
                break;
            exch(a, lp, hp);
        }
        exch(a, l, hp);
        return hp;
    }


    public static boolean less(int a, int b) {
        return a < b;
    }

    public static void exch(int[] a, int i, int j) {
        int tem = a[i];
        a[i] = a[j];
        a[j] = tem;
    }
}
