import java.util.Arrays;
import java.util.Scanner;

public class ExchangeDrink {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        int x = reader.nextInt();
        int[] array = new int[x];
        for (int i = 0; i < x; i++) {
            array[i] = reader.nextInt();
        }
        int y = reader.nextInt();
        System.out.println(minCoins(array, y));
    }


    public static int minCoins(int[] arr, int aim) {
        Arrays.sort(arr);
        if (arr.length==0)
            return -1;
        // 最小数的两倍大于则不行
        if(arr[0]*2>aim)
            return -1;
        int result = 0;
        int left =aim;
        for(int i=arr.length-1;i>0;i--){
            if (left<=0){
                break;
            }
            while(left>=arr[i]){
                left-=arr[i];
                result++;
            }
        }
        if (left!=0)
            return -1;
        return result;


    }
}