import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class 区间 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        int t = scanner.nextInt();
        scanner.nextLine();
        int[] nums = new int[n];
        for (int i = 0; i < n; i++) {
            nums[i] = scanner.nextInt();
        }

        int result = 0;
        int i = k - 1;
        int j = 0;
        if (k>n || t>k){
            System.out.println(0);
            return;
        }

        while (i < n) {
            HashMap<Integer, Integer> map = new HashMap();

            for (int index = j; index <= i; index++) {
                if (map.get(nums[index]) == null) {
                    map.put(nums[index], 1);
                    continue;
                }
                map.put(nums[index], map.get(nums[index]) + 1);
            }

            result += isValidate(t, map);

            i++;
            j++;
        }
        System.out.println(result);
    }

    private static int isValidate(int t, Map<Integer, Integer> map) {
        for (Integer key : map.keySet()) {
            if (map.get(key) >= t)
                return 1;
        }
        return 0;
    }
}
