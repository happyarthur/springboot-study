import java.util.Scanner;

public class 鸡鸭 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        char[] chars = str.toCharArray();
        char[] chars1 = str.toCharArray();
        char[] chars2 = str.toCharArray();
        char[] chars3 = str.toCharArray();
        char[] chars4 = str.toCharArray();

        // C 全左边的排列方式 只移动C
        int bushu1 = 0;
        for (int i=0;i<chars1.length;i++){
            if (chars1[i]=='D')
                continue;
            bushu1+=toLeft(chars1,i,'C');
        }
        // C 全左边的排列方式 只移动D
        int bushu2 = 0;
        for (int i=chars2.length-1;i>=0;i--){
            if (chars2[i]=='C')
                continue;
            bushu2+=toRight(chars2,i,'D');
        }
        // D 全左边的排列方式 只移动D
        int bushu3 = 0;
        for (int i=0;i<chars3.length;i++){
            if (chars3[i]=='C')
                continue;
            bushu3+=toLeft(chars3,i,'D');
        }
        // D 全左边的排列方式 只移动C
        int bushu4 = 0;
        for (int i=chars4.length-1;i>=0;i--){
            if (chars[i]=='D')
                continue;
            bushu4+=toRight(chars4,i,'C');
        }

        System.out.println(Math.min(Math.min(bushu1,bushu2),Math.min(bushu3,bushu4)));
    }

    // 把index的移动到最右边知道出现指定字符的位置
    public static int toRight(char[] chars,int index,char untilChar){
        if (index>=(chars.length-1))
            return 0;
        if (chars[index+1]==untilChar)
            return 0;
        swap(chars,index,index+1);
        return 1+toRight(chars,index+1,untilChar);
    }

    public static int toLeft(char[] chars,int index,char untilChar){
        if (index<=0)
            return 0;
        if (chars[index-1]==untilChar)
            return 0;
        swap(chars,index,index-1);
        return 1+toLeft(chars,index-1,untilChar);
    }

    public static void swap(char[] chars,int a,int b){
        char c = chars[a];
        chars[a] = chars[b];
        chars[b] = c;
    }

}