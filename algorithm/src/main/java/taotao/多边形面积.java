package taotao;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class 多边形面积 {

    static class Point {
        public double x;
        public double y;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return ""+x+" "+y;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.valueOf(scanner.nextLine());
        Queue<Point> points = new PriorityQueue<>(10000, new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                if (o1.x - o2.x > 0.00001)
                    return (int) ((o1.x - o2.x) * 10000 % Integer.MAX_VALUE);
                return (int) ((o1.y - o2.y) * 10000 % Integer.MAX_VALUE);
            }
        });

        for (int i = 0; i < n; i++) {
            String s = scanner.nextLine();
            double x = Double.valueOf(s.split(" ")[0]);
            double y = Double.valueOf(s.split(" ")[1]);
            points.add(new Point(x, y));
        }


        List<Point> sjx = new ArrayList<>();
        sjx.add(points.poll());
        sjx.add(points.poll());
        sjx.add(points.poll());
        double s = countS(sjx);
        while (!points.isEmpty()){
            sjx.set(0,sjx.get(1));
            sjx.set(1,sjx.get(2));
            sjx.set(2,points.poll());
            s += countS(sjx);
        }
        if ((long)s+1 - s < 0.0001)
            s+=1;
        System.out.println((long) s);

    }

    public static double countS(List<Point> points){
        return countS(points.get(0),points.get(1),points.get(2));
    }
    public static double countS(Point ap,Point bp,Point cp){
        double a = lineLength(ap,bp);
        double b = lineLength(ap,cp);
        double c = lineLength(bp,cp);
        double s=(a+b+c)/2;
        double area=Math.sqrt(s*(s-a)*(s-b)*(s-c));
        return area;
    }

    public static double lineLength(Point a,Point b){
       return Math.sqrt((a.x - b.x)*(a.x - b.x)+(a.y-b.y)*(a.y-b.y));
    }

}
