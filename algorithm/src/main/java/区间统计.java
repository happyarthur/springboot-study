
import java.util.HashMap;
import java.util.Scanner;


public class 区间统计 {

    public static boolean isValidate(HashMap<Integer, Integer> map, int t) {
        for (Integer key : map.keySet()){
            if (map.get(key) >= t){
                return true;
            }
        }
        return false;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int k = scanner.nextInt();
        int t = scanner.nextInt();


        int[] nums = new int[n];
        for (int i = 0; i < n; i++) {
            nums[i] = scanner.nextInt();
        }

        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < k; i++){
            map.put(nums[i], map.getOrDefault(nums[i], 0) + 1);
        }
        int count = 0;
        if (isValidate(map, t))
            count++;
        for (int i = k; i < n; i++) {
            map.put(nums[i-k], map.get(nums[i-k]) - 1);
            map.put(nums[i], map.getOrDefault(nums[i], 0) + 1);
            if (isValidate(map, t)){
                count++;
            }
        }
        System.out.println(count);
    }


}