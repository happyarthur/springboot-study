public class 回文数相关 {


    public static void main(String[] args) {
        deleteMaxChar2Huiwen("googlegooglegoogle");
    }


    // 一个字符串删除最少的数使其成为回文数
    private static void deleteMaxChar2Huiwen(String str){
        String reStr = new StringBuffer(str).reverse().toString();
        int maxCommonSubStr = maxCommonSubStr(str,reStr);
        System.out.println(str.length()-maxCommonSubStr);
    }

    // 求最大公共子串
    private static int maxCommonSubStr(String str1,String str2){
        int[][] dp = new int[str1.length()+1][str2.length()+1];
        for (int i=1;i<=str1.length();i++){
            for (int j=1;j<=str2.length();j++){
                dp[i][j] = str1.charAt(i-1)==str2.charAt(j-1) ? dp[i-1][j-1]+1 : Math.max(dp[i][j-1],dp[i-1][j]);
            }
        }
        return dp[str1.length()][str2.length()];
    }


}
