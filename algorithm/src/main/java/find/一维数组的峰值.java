package find;

public class 一维数组的峰值 {
    // 1 2 3 5 4 1 7 其中 5 7 就是局部峰值
    // 核心思想，任意分成两边，取边界上大的元素的那边 都会存在一个局部峰值
    public static void main(String[] args) {
        int[] a = {1,2,1,3,5,6,4};
        System.out.println(findPeakElement(a));
    }
    public static int findPeakElement(int[] nums) {
        int low = 0;
        int high = nums.length-1;
        while(low < high)
        {
            int mid1 = (low+high)/2;
            int mid2 = mid1+1;
            if(nums[mid1] < nums[mid2])
                low = mid2;
            else
                high = mid1;
        }
        return low;

    }
}
