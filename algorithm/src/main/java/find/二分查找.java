package find;

public class 二分查找 {
    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println(findN(a, 9));
        System.out.println(findN(a, 8));
        System.out.println(findN(a, 1));
        System.out.println(findN(a, 0));
    }

    // 找到n的位置 没有就返回-1 数组从小到大输入
    private static int findN(int[] a, int n) {
        int low = 0;
        int high = a.length - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (low == high)
                return a[low] == n ? low : -1;
            if (a[mid] == n)
                return mid;
            if (a[mid] > n) {
                high = mid - 1;
                continue;
            }
            low = mid + 1;
        }
        return -1;
    }
}
