import java.math.BigDecimal;
import java.util.Scanner;

public class Test {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        BigDecimal[] bigDecimalList = new BigDecimal[n+10];

        bigDecimalList[1] = getPureNum(1);
        bigDecimalList[2] = getPureNum(2);

        for (int i=3;i<=n;i++){
            BigDecimal now = new BigDecimal(0);
            now.add(getPureNum(i));
            now.add(getKuoHaoNum(i));
            now.add(getLink(n,bigDecimalList));
            bigDecimalList[i] = now;
        }

        System.out.println(bigDecimalList[n].divideAndRemainder(new BigDecimal(1000000007))[1].toString());


    }


    public static BigDecimal getLink(int n,BigDecimal[] bigDecimalList){
        BigDecimal result = new BigDecimal(0);
        if (n<3)
            return result;
        n--;

        for(int i=1;i<n;i++){
            result.add(bigDecimalList[i].multiply(bigDecimalList[n-i]).multiply(new BigDecimal(2)));
        }
        return result;
    }

    public static BigDecimal getKuoHaoNum(int n){
        BigDecimal result = new BigDecimal(0);
        if (n<3)
            return result;
        for (int i=n-2;i>0;i-=2){
            result.add(getPureNum(i));
        }
        return result;
    }

    // 纯数字组合
    public static BigDecimal getPureNum(int n){
        BigDecimal result = new BigDecimal(1);
        BigDecimal b = new BigDecimal(10);
        if (n==0)
            return new BigDecimal(0);
        for (int i=0;i<n;i++)
            result = result.multiply(b);
        return result;
    }
}
