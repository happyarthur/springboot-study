package base;

public class 素数表 {

    public static void main(String[] args) {
        boolean[] notSushu = sushubiao(1000);
        System.out.println(notSushu[9]);
        System.out.println(notSushu[10]);
        System.out.println(notSushu[13]);
        System.out.println(notSushu[17]);
        System.out.println(notSushu[23]);
        System.out.println(notSushu[37]);
    }


    // n以内的素数表 0表示素数 1表示 非素数
    private static boolean[] sushubiao(int n) {
        n++;
        boolean[] notSushu = new boolean[n];
        if (n <= 2)
            return notSushu;
        for (int i = 2; i <= (n / 2); i++) {
            if (notSushu[i])
                continue;
            for (int j = 2; (j * i) < n; j++) {
                notSushu[i * j] = true;
            }
        }
        return notSushu;
    }
}
