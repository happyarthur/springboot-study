package base;

//最大公约数
public class Gcd {

    public static void main(String[] args) {
        System.out.println(gcd(10, 5));
        System.out.println(gcd(5, 10));
        System.out.println(gcd(1, 1));
        System.out.println(gcd(1, 2));
        System.out.println(gcd(2, 3));
    }

    private static int gcd(int a, int b) {
        if (b == 0) return a;
        return gcd(b, a % b);
    }

    //公倍数
    private static int gbs(int a, int b) {
        return a * b / gcd(a, b);
    }
}
