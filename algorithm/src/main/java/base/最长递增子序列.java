package base;

import java.util.Arrays;

public class 最长递增子序列 {

    // 思想 去一个数组 B
    //  B[0] = 1 表示长度为1（0+1）的递增子序列，结尾为1
    //  B[1] = 3 表示长度为2（1+1）的递增子序列，结尾为3
    // 核心公式 当一个新的数(a)大于等于末尾的数(b)  B[++n] = a
    // 否则在B[0~n]中找到刚比他大的数字，替换掉
    // 因为是有序的 所以可以用二分查找来找到这个位置来替换
    // O(nlogn) 复杂度
    public static void main(String[] args) {
        System.out.println(findLongest(2, 1, 5, 3, 6, 4, 8, 9, 7));
    }

    public static int findLongest(int... A) {
        int length = A.length;
        int[] B = new int[length];
        B[0] = A[0];
        int end = 0;
        for (int i = 1; i < length; ++i) {
            // 如果当前数比B中最后一个数还大，直接添加
            if (A[i] >= B[end]) {
                B[++end] = A[i];
                continue;
            }
            // 否则，需要先找到替换位置
            int pos = findInsertPos(B, A[i], 0, end);
            B[pos] = A[i];
        }
        System.out.println(Arrays.toString(B));
        return end + 1;
    }

    /**
     * 二分查找第一个大于等于n的位置
     */
    private static int findInsertPos(int[] B, int n, int start, int end) {
        while (start < end) {
            int mid = start + (end - start) / 2;// 直接使用(high + low) / 2 可能导致溢出
            if (B[mid] < n) {
                start = mid + 1;
            } else if (B[mid] > n) {
                end = mid;
            } else {
                return mid;
            }
        }
        return start;
    }
}
