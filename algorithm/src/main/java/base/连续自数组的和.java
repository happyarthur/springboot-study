package base;

// 核心思想：贪心算法，如果前面的数字加起来以已经小于0了那么就舍弃他们
public class 连续自数组的和 {


    public static void main(String[] args) {
        int[] a = {6,-3,-2,7,-15,1,2,2};
        System.out.println(maxSum(a));
        System.out.println(findGreatestSumOfSubArray(a));
    }

    private static int maxSum(int[] a) {
        if (a.length <= 0)
            return Integer.MIN_VALUE;
        if (a.length == 1)
            return a[1];
        int max = a[0];
        int sum = a[0];
        if (sum<=0)
            sum = 0;
        for (int i = 1; i < a.length; i++) {
            sum += a[i];
            max = Math.max(max,sum);
            if (sum<=0)
                sum = 0;
        }
        return max;
    }

    public static int findGreatestSumOfSubArray(int[] nums) {
        if (nums.length == 0) return 0;
        int ret = Integer.MIN_VALUE;
        int sum = 0;
        for (int val : nums) {
            if (sum <= 0) sum = val;
            else sum += val;
            ret = Math.max(ret, sum);
        }
        return ret;
    }

}
