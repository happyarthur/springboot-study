package base;

public class 最长公共子序列 {

    public static void main(String[] args) {
        String a = "google";
        String b = "gooasd";
        System.out.println(zixulie(a, b));
    }
    //核心思想 动态规划dp[i][j] 表示s1[0~i] 和s2[0~j]的最长公共子序列
    // 核心 ： 如果s1[i]==s2[j] 那么肯定是dp[i-1][j-1]+1 否则就是dp[i-1][j] 和 dp[i][j-1] 中大的那个
    private static int zixulie(String s1, String s2) {
        int[][] dp = new int[s1.length() + 1][s2.length() + 1];
        for (int i = 1; i < dp.length; i++) {
            for (int j = 1; j < dp[0].length; j++) {
                dp[i][j] = s1.charAt(i - 1) == s2.charAt(j - 1) ? dp[i - 1][j - 1] + 1 : Math.max(dp[i - 1][j], dp[i][j - 1]);
            }
        }
        return dp[dp.length - 1][dp[0].length - 1];
    }

}
