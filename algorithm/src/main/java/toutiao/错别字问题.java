package toutiao;

import java.util.Scanner;
import java.util.TreeMap;

public class 错别字问题 {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        int linesNum = Integer.valueOf(s.nextLine());
        TreeMap<Integer, Integer> flags = new TreeMap();
        for (int i = 0; i < linesNum; i++) {
            String str = s.nextLine();
            for (String str1 : str.split(";")) {
                String a = str1.split(",")[0];
                String b = str1.split(",")[1];
                flags.put(Integer.valueOf(a), Integer.valueOf(b));
            }
        }


        int start = -1;
        int end = -1;
        boolean f = true;
        boolean newLine = true;
        for (int key : flags.keySet()) {
            if (newLine) {
                if (f) {
                    f = false;
                    start = key;
                    end = flags.get(key);
                }
                newLine=false;
            }
            if (key <= end) {
                int temp = flags.get(key);
                if (temp < end)
                    continue;
                end = temp;
                continue;
            }

            System.out.print(start + "," + end+";");
            start = key;
            end = flags.get(key);
            newLine=true;
        }

        System.out.print(start+","+end);

    }
}
