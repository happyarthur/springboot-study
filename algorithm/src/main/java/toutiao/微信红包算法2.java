package toutiao;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 i=1 money=899966
 i=2 money=878624
 i=3 money=867056
 i=4 money=849447
 i=5 money=827584
 i=6 money=677323
 */
// 这个算法大致的是公平的
public class 微信红包算法2 {

    private static Map<Integer, Long> tongji = new LinkedHashMap<>();

    public static void main(String[] args) {

//        moni(5,5);
//        moni(6,5);
//        moni(6,500);
//        moni(6,500);
//        moni(6,500);

        for (int i = 0; i < 100000; i++) {
            moni(10, 100);
        }
        for (Integer i : tongji.keySet()) {
            System.out.print("i=" + i);
            System.out.print(" money=" + tongji.get(i));
            System.out.println("");
        }

    }

    private static void moni(int nums, int fee) {
        if (nums > fee) {
            System.out.println("每个人的金额不能小于0.1");
//            System.out.println("============");
            return;
        }
        int max = nums;
        for (int i = 1; i < (max + 1); i++) {
            int fee_temp = getFee(nums, fee);
            fee -= fee_temp;
            nums--;
//            System.out.println("第" + i + "个人抢到的钱:" + fee_temp / 100.0);
            tongjiAdd(i, fee_temp);
        }
//        System.out.println("============");

    }

    // remain 还有多少人没有抢 fee 还剩多少钱 单位分
    private static int getFee(int remain, int fee) {
        if (remain == 1) {
            return fee;
        }
        int max = fee / remain * 2;

        long a = Math.round(Math.random() * (max)) + 1;
        return (int) a;
    }

    private static void tongjiAdd(Integer key, Integer val) {
        tongji.put(key, tongji.getOrDefault(key, 0L) + val);
    }
}
