package dp;

/**
 * 题目描述：For example, given n = 2, return 1 (2 = 1 + 1); given n = 10, return 36 (10 = 3 + 3 + 4).
 * 给出一个数字n，这个数字可分为几个数字的和，将这几个数字相乘的到m，求m的最大值
 */
public class 分割整数的最大乘积 {

    public static void main(String[] args) {
        System.out.println(intBreak(6));
        System.out.println(intBreak(10000));
    }

    private static int intBreak(int n) {
        int[] dp = new int[n + 1];// 第i个表示数i的最大乘积
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            for (int j = 1; j < i; j++) {
                dp[i] = Math.max(dp[i], Math.max(dp[i - j] * j, j * (i - j)));
            }
        }
        return dp[n];
    }
}
