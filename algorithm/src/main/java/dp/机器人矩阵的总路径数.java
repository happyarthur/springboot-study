package dp;

/**
 * m*n的矩阵 m为高 n为长 机器人从（0，0）开始 每次只能向右或者向下走
 * 求到右下角 有几种走法
 */
public class 机器人矩阵的总路径数 {

    public static void main(String[] args) {
        System.out.println(count(2,2));
    }


    private static int count(int m, int n) {
        int[] dp = new int[n+1];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i==0){
                    dp[j+1] = 1;
                    continue;
                }
                dp[j+1] += dp[j];
            }
        }
        return dp[n];
    }
}
