import java.util.Stack;

public class 二叉树相关 {

    static class TreeNode {
        public TreeNode left;
        public TreeNode right;
        public int val;

        TreeNode(int val) {
            this.val = val;
        }
    }

    public static void main(String[] args) {
        TreeNode node = genNode(new TreeNode(1));
        bianli(node);
        System.out.println("");
        System.out.println("------------");
        bianliWhile(node);
    }

    // 非递归遍历
    private static void bianliWhile(TreeNode node){
        Stack<TreeNode> stack = new Stack<>();
        stack.push(node);
        while (!stack.isEmpty()){
            TreeNode n = stack.pop();
            if (n==null)
                continue;
            stack.push(n.right);
            stack.push(n.left);
            System.out.print(n.val+",");
        }
    }

    // 递归遍历
    private static void bianli(TreeNode node){
        if (node==null)
            return;
        System.out.print(node.val+",");
        bianli(node.left);
        bianli(node.right);
    }


    // 生成测试二叉树
    private static int MAX = 20;

    private static TreeNode genNode(TreeNode node) {
        if (node == null) return node;
        if (MAX <= 0)
            return node;
        MAX -= 2;
        node.left = new TreeNode((int) (Math.random() * 100));
        node.right = new TreeNode((int) (Math.random() * 100));
        genNode(node.left);
        genNode(node.right);
        return node;
    }
}
