package hello;

import hello.aop.introduction.Animal;
import hello.aop.introduction.Woman;
import hello.aop.weaving.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

    @Autowired
    Woman woman;
    @Autowired
    Player player;

    /**
     * aop 切入测试
     * @return
     */
    @RequestMapping("/introduction")
    public String index() {
        String s = "";
        s += woman.talk() + "<br/>";
        s += ((Animal) woman).eat() + "<br/>";
        return "introduction演示<br/>" + s;
    }

    /**
     * aop 织入测试（无参数）
     * @return
     */
    @RequestMapping("/weaving")
    public String aop1() {
        return player.play();
    }

    /**
     * aop 织入测试 带参数
     * @return
     */
    @RequestMapping("/weaving-args")
    public String aop2() {
        return player.play("沙雕",233,94);
    }

}
