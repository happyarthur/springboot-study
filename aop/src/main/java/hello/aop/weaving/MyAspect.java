package hello.aop.weaving;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyAspect {


    /**
     *
     * 这个方法执行什么都无所谓，不会用到，只是 Pointcut 的载体
     * <p>
     * 这个方法 public和private都无所谓
     */
    @Pointcut(value = "execution(* hello.aop.weaving.Player.play())")
    public void playPoint() {
        System.out.println("这里不会被执行");
    }

    /**
     * 1. 这些切面不管是around方法执行还是其他人执行 只要执行一次 Player.play()方法就会按照规则调用一次
     */
    @Before("playPoint()")
    private void takeSeats() {
        System.out.println("在小白打游戏前，小黑搬来了位置");
    }

    @After("playPoint()")
    private void leaveSeats() {
        System.out.println("打完游戏，小黑看完走了");
    }

    @AfterReturning("playPoint()")
    private void afterReturn() {
        System.out.println("play()返回后调用");
    }


    @AfterThrowing("playPoint()")
    private void afterThrowing() {
        System.out.println("play() 抛出异常后调用");
    }


    /**
     * 这个的理解 在于拦截，完全获得方法执行的自由权
     * 如果不执行 jb.proceed（）方法将一直阻塞
     * 相当于 watchPlay()方法将原来方法包裹，watchPlay()的返回值 将直接替换倒原来函数的位置
     *
     * @param jb
     * @return
     */
    @Around("playPoint()")
    public Object watchPlay(ProceedingJoinPoint jb) {
        try {
            System.out.println("[watchPlay]如果不调用jb.preceed（）那么原来的方法就一直阻塞");
            System.out.println("[watchPlay]执行之前");
            jb.proceed();
            System.out.println("[watchPlay]执行之后");
            System.out.println("[watchPlay]第二次执行");
            jb.proceed();
            System.out.println("[watchPlay]第二次执行之后");
            throw new Exception();
        } catch (Throwable e) {
            System.out.println("[watchPlay]执行出现异样");
        }

        try {
            System.out.println("没有这个返回就不会返回");
            return jb.proceed() + "|我经历了around的拦截";
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    /////////////////////////////////////带参数的织入//////////////////////////////////////

    @Pointcut(value = "execution(* hello.aop.weaving.Player.play(String,int,Integer)) && args(a,b,c)")
    public void playArgsPoint(String a, int b, Integer c) {
    }

    @Before("playArgsPoint(a,b,c)")
    private void takeSeats(String a, int b, Integer c) {
        a = "a=" + a + "  b=" + b + "   c=" + c;
        System.out.println("在小白打游戏前，小黑搬来了位置 arg=" + a);
    }

    @After("playArgsPoint(a,b,c)")
    private void leaveSeats(String a, int b, Integer c) {
        a = "a=" + a + "  b=" + b + "   c=" + c;
        System.out.println("打完游戏，小黑看完走了arg=" + a);
    }

    @AfterReturning("playArgsPoint(a,b,c)")
    private void afterReturn(String a, int b, Integer c) {
        a = "a=" + a + "  b=" + b + "   c=" + c;
        System.out.println("play()返回后调用arg=" + a);
    }

    @AfterThrowing("playArgsPoint(a,b,c)")
    private void afterThrowing(String a, int b, Integer c) {
        a = "a=" + a + "  b=" + b + "   c=" + c;
        System.out.println("play() 抛出异常后调用arg=" + a);
    }


    @Around("playArgsPoint(a,b,c)")
    public Object watchPlayArgs(ProceedingJoinPoint jb, String a, int b, Integer c) {
        try {
            a = "a=" + a + "  b=" + b + "   c=" + c;
            System.out.println("[watchPlayArgs]如果不调用jb.preceed（）那么原来的方法就一直阻塞");
            System.out.println("[watchPlayArgs]执行之前");
            jb.proceed();
            System.out.println("[watchPlayArgs]执行之后");
            System.out.println("[watchPlayArgs]第二次执行");
            jb.proceed();
            System.out.println("[watchPlayArgs]第二次执行之后 args=" + a);
            throw new Exception();
        } catch (Throwable e) {
            System.out.println("[watchPlayArgs]执行出现异样");
        }

        try {
            System.out.println("没有这个返回就不会返回");
            return jb.proceed() + "|我经历了watchPlayArgs around的拦截";
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    /////////////////////////////匹配所有函数/////////////////////////////////////

    @Pointcut(value = "execution(* hello.aop.weaving.Player.play(..))")
    public void playAll() {
    }

    @Before("playAll()")
    private void beforePlayAll() {
        System.out.println(">>>>>>匹配所有的play()函数<<<<<<");
    }

}
