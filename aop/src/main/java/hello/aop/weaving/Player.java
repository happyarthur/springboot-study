package hello.aop.weaving;

import org.springframework.stereotype.Component;

/**
 * 业务类，正常执行业务
 */
@Component
public class Player {
    String name;

    public Player() {
        this.name = "玩家小白";
    }

    public String play() {
        String s = name + "玩游戏";
        System.out.println(s);
        return "织入调用测试 "+s;
    }

    public String play(String a, int b, Integer c) {
        a = "a=" + a + "  b=" + b + "   c=" + c;
        System.out.println(a);
        return "带参数的织入调用测试[play] " + a;
    }

}
