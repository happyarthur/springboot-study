package hello.aop.introduction.aspect;

import hello.aop.introduction.Animal;
import hello.aop.introduction.DefaultAnimal;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.DeclareParents;
import org.springframework.stereotype.Component;


@Component
@Aspect
/**
 * 用来创建代理 将Woman & DefaultAnimal 两个类结合，用来给Woman添加Animal接口的功能
 */
public class AspectIntroductionConfig {
    @DeclareParents(value = "hello.aop.introduction.Woman", defaultImpl = DefaultAnimal.class)
    public static Animal animal;
}


