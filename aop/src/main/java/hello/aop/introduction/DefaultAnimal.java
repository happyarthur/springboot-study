package hello.aop.introduction;

import org.springframework.stereotype.Component;

@Component
public class DefaultAnimal implements Animal {
    @Override
    public String eat() {
        String s = "DefaultAnimal 代理类动物 给女人提供吃的方法";
        System.out.println(s);
        return s;
    }
}
