package hello.aop.introduction;

import org.springframework.stereotype.Component;

@Component
public class Woman implements Human {
    @Override
    public String talk() {
        String s = "女人在说话";
        System.out.println(s);
        return s;
    }
}
