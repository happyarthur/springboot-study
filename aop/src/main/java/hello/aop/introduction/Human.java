package hello.aop.introduction;

public interface Human {
    public String talk();
}
