package hello.aop.introduction;

public interface Animal {
    String eat();
}
