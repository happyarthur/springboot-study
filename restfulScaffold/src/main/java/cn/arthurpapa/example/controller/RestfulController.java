package cn.arthurpapa.example.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class RestfulController {

    // "api/search/mixed;keywords=a,b,c"
    @RequestMapping(value = "/{searchType}", method = RequestMethod.GET)
    public List<String> search(@PathVariable String searchType, @RequestParam(defaultValue = "Papa", required = true) String name) {
        List result = new ArrayList();
        result.add("SearchType: " + searchType);
        result.add("name: " + name);
        return result;
    }
}
