package cn.arthurpapa.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import springfox.documentation.annotations.Cacheable;

@Controller
@RequestMapping("/hello")
public class HelloController {

    // http://localhost:8080
    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String hello() {
        return "resultPage";
    }

    // http://localhost:8080/1
    @RequestMapping(value = "/1",method = RequestMethod.GET)
    public String hello1(Model model) {
        model.addAttribute("message", "来自服务器的关怀");
        return "resultPage2";
    }

    // http://localhost:8080/2?name=Arthur
    @Cacheable("yourCacheName1")
    @RequestMapping(value = "/2",method = RequestMethod.GET)
    public String hello3(Model model, @RequestParam(defaultValue = "Papa",required = true) String name) {
        model.addAttribute("message", "Hello," + name);
        return "resultPage2";
    }
}
