package cn.arthurpapa.example.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/security")
public class SecurityTestController {

    @RequestMapping(value = "/1",method = RequestMethod.GET)
    public String hello() {
        return "user,admin可以访问";
    }
    @RequestMapping(value = "/1",method = RequestMethod.POST)
    public String hello1() {
        return "admin才能访问";
    }
}
