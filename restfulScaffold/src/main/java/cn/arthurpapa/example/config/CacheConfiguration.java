package cn.arthurpapa.example.config;

import com.google.common.cache.CacheBuilder;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * 配置应用缓存 为需要缓存的方法建立缓存，参数相同时下次访问的时候可以直接返回
 * 不限于Controller层，所有的方法都可以使用该缓存
 * 如果返回中有类没有实现可序列化接口，将无法缓存，需要手动实现
 */
@Configuration
@EnableCaching
public class CacheConfiguration {

    @Bean
    public CacheManager cacheManager(){
        GuavaCacheManager guavaCacheManager = new GuavaCacheManager("yourCacheName1","yourCacheName2");
        guavaCacheManager.setCacheBuilder(
                CacheBuilder.newBuilder()
                        .softValues()
                        .expireAfterWrite(10, TimeUnit.MINUTES));
        return guavaCacheManager;
    }

}
