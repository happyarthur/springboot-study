package cn.arthurpapa.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import java.time.Duration;
import java.util.Collections;
import java.util.Set;

// 如果使用redis profile 会优先使用redis作为缓存 做到分布式缓存
// 前提是已经配置好了redisTemplate
@Configuration
@Profile("redis")
public class RedisConfiguration {

    @Autowired
    RedisTemplate<Object, Object> redisTemplate;

    @Primary @Bean
    public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory){
        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(Duration.ofHours(1));
        Set names = Collections.EMPTY_SET;
        Collections.addAll(names,"yourCacheName1","yourCacheName2");
        RedisCacheManager cacheManager = RedisCacheManager.
                builder(RedisCacheWriter.nonLockingRedisCacheWriter(redisConnectionFactory)).
                cacheDefaults(redisCacheConfiguration).
                initialCacheNames(names).build();
        return cacheManager;
    }
}
