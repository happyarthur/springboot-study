package cn.arthurpapa.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

@Configuration
// EnableGlobalMethodSecurit注解允许我们在方法上添加注解来控制角色访问 如 @Secured("ROLE_NAME")
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    // 手动添加用户
    @Autowired
    public void configureAuth(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("user").password("user").roles("USER").and()
                .withUser("admin").password("admin").roles("USER", "ADMIN");
    }

    //因为5.x版本新增了多种密码加密方式，必须指定一种
    /**
     * 密码加密
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .csrf().disable() //Cross Site Request Forgery
                .authorizeRequests()
                .antMatchers("/login.html**", "/authentication/require",
                        "/authentication/form").permitAll() //对这些地址全开放
                .antMatchers(HttpMethod.GET, "security/**").hasRole("USER")
                .antMatchers(HttpMethod.POST, "security/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "security/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "security/**").hasRole("ADMIN")
                .anyRequest().authenticated();
    }
}
