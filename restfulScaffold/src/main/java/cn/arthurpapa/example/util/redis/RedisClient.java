package cn.arthurpapa.example.util.redis;

import cn.arthurpapa.example.util.CommonUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;


/**
 * Redis操作客户端
 */
@Service
public class RedisClient {

    public static final org.slf4j.Logger logger = LoggerFactory.getLogger(RedisClient.class);

    @Autowired
    RedisTemplate<Object, Object> redisTemplate;

    /**
     * 按照key获取redis中缓存值
     */
    public Object get(String key) {
        Object o = null;
        try {
            o = redisTemplate.opsForValue().get(key);
        } catch (Exception e) {

            logger.warn("redis 操作异常", e);
        }
        return o;
    }

    /**
     * 按照key获取redis中缓存值 获取序列化后存储后的内容反序列化
     * 否则会报错
     */
    public Object getObject(String key) {
        String o = null;
        try {
            o = (String) redisTemplate.opsForValue().get(key);
        } catch (Exception e) {
            logger.warn("redis 操作异常", e);
        }
        if (o == null)
            return o;
        return CommonUtils.objectDeserialization(o);
    }

    /**
     * 设置缓存值到redis中
     *
     * @param key   键
     * @param value 值
     * @param time  时间数值
     * @param unit  单位
     */
    public void set(String key, Object value, long time, TimeUnit unit) {

        try {
            redisTemplate.delete(key);
            redisTemplate.opsForValue().set(key, value, time, unit);
        } catch (Exception e) {
            logger.warn("redis 操作异常" + e);
        }
    }

    /**
     * 设置缓存值到redis中
     *
     * @param key   键
     * @param value 值
     * @param time  时间数值
     * @param unit  单位
     */
    public void setObject(String key, Object value, long time, TimeUnit unit) {
        String val = CommonUtils.objectSerialiable(value);
        try {
            redisTemplate.delete(key);
            redisTemplate.opsForValue().set(key, val, time, unit);
        } catch (Exception e) {
            logger.warn("redis 操作异常" + e);
        }
    }

    /**
     * 不存在时设置 value
     *
     * @param key   键
     * @param value 值
     * @return true 设置成功, false 设置失败
     */
    public boolean setIfAbsent(String key, Object value) {
        try {
            return redisTemplate.opsForValue().setIfAbsent(key, value);
        } catch (Exception e) {
            logger.warn("redis 操作异常" + e);
        }
        return false;
    }

    /**
     * 设置redis 数据的超时时间
     */
    public void setExpire(String key, final long timeout, final TimeUnit unit) {
        try {
            redisTemplate.expire(key, timeout, unit);
        } catch (Exception e) {
            logger.warn("redis 设置超时异常, key = [{}]", key, e);
        }
    }


    /**
     * 删除redis数据
     */
    public void deleteKey(String key) {
        try {
            redisTemplate.delete(key);
        } catch (Exception e) {
            logger.warn("redis 删除操作异常, key = [{}]", key, e);
        }
    }


    /**
     * 按照key获取redis队列中的缓存值
     */
    public Object pop(String key) {
        Object o = null;
        try {
            return redisTemplate.opsForList().rightPop(key);
        } catch (Exception e) {
            logger.warn("redis 操作异常" + e);
        }
        return o;
    }

    /**
     * 按照key获取redis队列中的缓存值
     */
    public Object push(String key, Object value) {
        Object o = null;
        try {
            redisTemplate.opsForList().leftPush(key, value);
        } catch (Exception e) {
            logger.warn("redis 操作异常" + e);
        }
        return o;
    }
}