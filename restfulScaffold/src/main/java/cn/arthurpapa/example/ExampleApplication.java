package cn.arthurpapa.example;

import cn.arthurpapa.example.config.InterceptorConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@MapperScan("com.o2study.co2.mapper")
public class ExampleApplication extends WebMvcConfigurerAdapter {

	public static void main(String[] args) {
		SpringApplication.run(ExampleApplication.class, args);
	}

	@Override
	//添加拦截器 解决跨域问题
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new InterceptorConfiguration()).addPathPatterns("/**");
		super.addInterceptors(registry);
	}
}
