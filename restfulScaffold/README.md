# Web脚手架

Web项目脚手架，整合了
1. Mybatis和Redis，添加了通过mysql表自动生成mybatis xml文件。
2. spring security
3. 配置了应用缓存 提供了guava和redis两种方式缓存
4. 拦截器的使用
5. swagger
6. thymeleaf
7. spring mvc restful形式